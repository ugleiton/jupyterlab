#https://github.com/jupyter/docker-stacks
FROM jupyter/datascience-notebook
COPY requirements.txt /opt/

RUN pip install -r /opt/requirements.txt

#https://github.com/jupyterlab/jupyterlab/issues/5492 - Feature request: File > New > Python File #5492
RUN jupyter labextension install jupyterlab-python-file

USER root
#javascript
RUN apt-get update  && \
    apt-get install -y nodejs npm  && \
    npm install -g --unsafe-perm ijavascript  && \
    ijsinstall --install=global && \
    chown -R "${NB_USER}" "/home/${NB_USER}/" "${CONDA_DIR}"
# others
RUN apt-get install -y graphviz

USER $NB_UID
RUN fix-permissions "${CONDA_DIR}" && \
    fix-permissions "/home/${NB_USER}" && \
    fix-permissions "${JULIA_PKGDIR}" "${CONDA_DIR}/share/jupyter"